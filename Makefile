TARGET = target/scala-2.12
JS := $(CURDIR)/$(TARGET)/battlecode2019-fastopt

all: compiled_bot.js

compiled_bot.js: $(TARGET)/battlecode2019-fastopt.js $(TARGET)/robot.js
	rm -f $@
	bc19compile -d $(TARGET)

$(TARGET)/battlecode2019-fastopt.js: FORCE
	sbt fastOptJS

run: compiled_bot.js
	bc19run --bc $^ -r examjs/

#This file needs an absolute path because I have no clue what directory
#bc19compile runs from
$(TARGET)/robot.js:
	echo "import { MyRobot } from '$(JS)'" > $@ 

FORCE: ;
