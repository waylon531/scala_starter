package bc19

import scala.scalajs.js
import scala.scalajs.js.annotation._

@js.native
trait Robot extends js.Object {
    val id: Int = js.native;
    val team: Int = js.native;
    val x: Int = js.native;
    val y: Int = js.native;
    val unit: Int = js.native;
    val turn: Int = js.native;
    val health: Int = js.native;
    val karbonite: Int = js.native;
    val fuel: Int = js.native;
    val signal: Int = js.native;
    val signal_radius: Int = js.native;
    val castle_talk: Int = js.native;
}
