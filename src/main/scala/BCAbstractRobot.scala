package bc19

import scala.scalajs.js
import scala.scalajs.js.annotation._

@js.native
@JSImport("battlecode","BCAbstractRobot")
class BCAbstractRobot extends js.Object {
    var logs: List[String] = js.native;
    var signal: Int = js.native;
    var signalRadius: Int = js.native;
    var castleTalk: Int = js.native;

    val me: Robot = js.native;
    val id: Int = js.native;
    val fuel: Int = js.native;
    val karbonite: Int = js.native;
    val lastOffer: List[List[Int]] = js.native;
    val map: List[List[Boolean]] = js.native;
    val karbonite_map: List[List[Boolean]] = js.native;
    val fuel_map: List[List[Boolean]] = js.native;

    def log(s: String): Unit = js.native
    def signal(value: Int, radius: Int): Unit = js.native
    def castleTalk(value: Int): Unit = js.native

    def attack(dx: Int, dy: Int): js.Any = js.native
    def buildUnit(unit: Int, dx: Int, dy: Int): js.Any = js.native
    def give(dx: Int, dy: Int, k: Int, f: Int): js.Any = js.native
    def proposeTrade(k: Int, f: Int): js.Any = js.native
    def mine(): js.Any = js.native
    def move(dx: Int, dy: Int): js.Any = js.native
    def turn(): js.Any = js.native

    def isVisible(robot: Robot): Boolean = js.native
    def isRadioing(robot: Robot): Boolean = js.native
    def getVisibleRobotMap(): List[List[Int]] = js.native
    def getPassableMap(): List[List[Boolean]] = js.native
    def getKarboniteMap(): List[List[Boolean]] = js.native
    def getFuelMap(): List[List[Boolean]] = js.native
    def getVisibleRobots(): List[Robot] = js.native
}

