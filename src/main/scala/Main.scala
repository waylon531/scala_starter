package bc19

import scala.scalajs.js
import scala.scalajs.js.annotation._

@JSExportTopLevel("MyRobot")
@ScalaJSDefined
class MyRobot extends BCAbstractRobot {
  override def turn(): js.Any  = {
    log("TEST")
    log(me.id.toString)
    return move(0,1)

  }
}
